# Assembly Writing some simple assembly fragments

Before we start writing functions in assembly language we will
start writing some simple assembly "fragments".  

**PAY ATTENTION:**
Each fragment
should have a symbolic label that identifies the beginning of its
instructions and the last instruction in the fragment should
trap back to the debugger (see [ending-exiting](https://appavooteaching.github.io/UndertheCovers/lecturenotes/assembly/L08.html#ending-exiting-our-program-process) from the lecture notes for more info).


Each fragment should be place in its own assembly file and assembled into
a reloctable elf file.  In the descriptions that follow each fragment you will be writing,
we inlcude the test code that will be using to test your fragment.  You should create a
test file of the appropriate name and place the test code in it and
test out your fragment.  To accomplish you should write a Makefile that creates
executables that links together your fragments and test object files.


The included test scripts expects your Makefile to have certain targets that you will need to write.  You will have to study the test script to figure out what these targets are.  HINT: Examine each line in the test script that invokes make.

Given that we don't know how to write complete functions yet our fragements finish by trapping back to the debugger.  With this in mind our code is "run" and tested using `gdb`.  For example each provided test script starts gdb with your executable and then runs gdb commands to excercise your fragement.  You should use the `gdb` by hand to explore both the testcode and your fragements.  Don't blindly use the test scripts they are there for you to explore and learn from.


## NOTES

Most of what you need is in the lecture slides and examples including the commands required to assemble your code and link it [example](https://appavooteaching.github.io/UndertheCovers/lecturenotes/assembly/L10.html#sumit-s-and-usesumit-s). If needed you can look up intel instructions and how they work in the Intel manual that are available online.  The table of contents lets you jump directly to the pages for a particular instruction. 

With respect to the address mode syntax for the operands.  You can find a summary and guidance in the [addressing modes lecture notes](https://appavooteaching.github.io/UndertheCovers/lecturenotes/assembly/L10.html#addressing-modes).  If you get lost ask questions on  piazza.

## The Fragments to write

There are three fragements `AND_FRAG`, `OR_FRAG` and `SUM_FRAG`.  Each are described below. 

### File: `and.s` Fragment symbol: `AND_FRAG`

#### Description
```
	# AND_FRAG Fragement
	# INPUTS: rax -> x
	#         rbx -> &y address of where in memory y is
	# OUTPUTS: x = x bitwise and y : update rax with bit wise and of the 
	#                                8 byte quantity at the location of &y
	#          rbx should be updated to equal &y + 8
```


#### Test code: `andtest.s`
```
	.intel_syntax noprefix
	.section .text
	.global _start
_start:
test1:
	mov rbx, OFFSET data_start
	mov rax, 0xdeadbeefdeadbeef
	jmp AND_FRAG
test2:	
	jmp AND_FRAG
test3:	
	jmp AND_FRAG

	.section .data
data_start:
	.quad -1                  # mask all bits
	.quad 0xaaaaaaaaaaaaaaaa  # every other bit
	.quad 0xF000F000F000F000
data_end:
	.quad 0x0
```

Study the test code to figure out exactly what is expected of your AND
routine.  You are encouraged to write your own more complex test program to ensure that you understand what is going on.  The autograder  on gradesscope will run your code against the above using its own version via the `addtest.sh`.

The included `addtest.sh` will be used to test your solution. It
assumes that you have created a Makefile that does what is needed. You
will need to study the `addtest.sh` to figure out what your makefile
will need to do and how your code will be tested with `gdb`.  Do not
blindly try and make your solution work with the test script.  Rather
read the scripts and figure out how to writing things and test them
youselves by hand.


### File: `or.s` Fragment Symbol: `OR_FRAG`

Similar to above but with or.  

#### Description

```
        # INPUTS: rax -> x
        #         rbx -> &y address of where in memory y is
        # OUTPUTS: x = x bitwise or y : update rax with bitwise or of the 
        #                               8 byte quantity at the location of &y
        #          rbx should be updated to equal &y + 8
```

#### Test code: `ortest.s`
```
        .intel_syntax noprefix
        .section .text
        .global _start
_start:
test1:
        mov rbx, OFFSET data_start
        xor rax, rax
        jmp OR_FRAG
test2:
        jmp OR_FRAG
test3:
        jmp OR_FRAG

        .section .data
data_start:
        .quad 1                  # lowest bit
        .quad 0x8000000000000000 # higest bit
        .quad 0x00000FFFFFF00000 # middle 12
data_end:
        .quad 0x0
```

### File: `sum.s`  Fragement Symbol: `SUM_FRAG`

Now that we have gotten the hang of the mechanics lets write a fragment that is a little more involved and operates on memory as well as registers.

#### Description
```
        # INPUTS: rax -> x
        #         rbx -> &y address of where in memory y is
        # OUTPUTS: x = x + y : update rax by adding y
        #                      quantity at the location of &y
        #          if y is positive then add y into an 8 byte value
        #          at stored at a location marked by a symbol
        #          named SUM_POSTIVE
        #          else add y into an 8 byte value stored at a 
        #          location makred by a symbol named SUM_NEGATIVE
        #          final rbx should be updated to equal &y + 8
        #
        # This file must provide the symbols SUM_POSTIVE 
        # and SUM_NEGATIVE and associated memory
```        


#### Test code: `sumtest.s`

```
        .intel_syntax noprefix

        .section .text
        .global _start
_start:
test1:
        xor rax, rax
        mov QWORD PTR [SUM_POSITIVE], rax
        mov QWORD PTR [SUM_NEGATIVE], rax

        mov rbx, OFFSET data_start

        jmp SUM_FRAG
test2:
        jmp SUM_FRAG
test3:
        jmp SUM_FRAG
test4:
        jmp SUM_FRAG

        .section .data
data_start:
        .quad -1
        .quad 1
        .quad 54644566
        .quad -2233
data_end:
        .quad 0x0
```